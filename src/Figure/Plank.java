package Figure;


import java.util.ArrayList;

class Plank {
    private static Plank instance;

    private Plank() {
    }

    static Plank getInstance() {
        if (instance == null) {
            instance = new Plank();
        }
        return instance;
    }

    private ArrayList<Shape> list = new ArrayList<>();            //массив который в будущем будет проверяться на максимум 4 элемента

    void put(Shape shape, int a) {
        if (list.size() == 0) {
            for (int i = 0; i < 4; i++) {
                list.add(null);
            }
        }
        a -= 1;
        if (list.size() > 4) {
            System.out.println("Доска заполнена");
        } else {
            if (a < 4 && a >= 0) {
                if (list.get(a) == null) {
                    System.out.println("Фигура " + shape.getClass().getName() + " установлена");
                    list.set(a, shape);
                } else {
                    System.out.println("Это место занято");
                }
            } else {
                System.out.println("Введите правильную цифру от 1 до 4");
            }
        }
    }

    void outPut(int num) {
        num -= 1;
        if (list.size() == 0) {
            for (int i = 0; i < 4; i++) {
                list.add(null);
            }
        }
        if (list.get(num) != null) {
            System.out.println(list.get(num).getClass().getName() + " больше не на доске");
            list.set(num, null);
        }
    }

    String infoAboutPlank() {
        String ans;
        double result = 0;
        for (Shape shape : list) {
            if (shape != null) {
                result += shape.getArea();
            }
        }
        ans = String.valueOf(result);
        return ans;
    }

    String infoAboutPlace() {
        String ans = " ";
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) != null) {
                ans = ans.concat(String.valueOf(i + 1)) + ";";
            }
        }
        return ans;
    }
}

