package Figure;

class Circle extends Shape {

    Circle(double x) {
        this.point = new Point(x);
    }

    private Point point;

    @Override
    double getPerimetr() {
        return point.getX() * 2 * 3.1415;
    }

    @Override
    double getArea() {
        return 3.1415 * Math.pow(point.getX(), 2);
    }
}
