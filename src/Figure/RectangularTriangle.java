package Figure;

class RectangularTriangle extends Shape {

    RectangularTriangle(double x, double y) {
        this.point = new Point(x, y);
    }

    private Point point;

    @Override
    double getPerimetr() {
        double c = Math.sqrt(Math.pow(point.getX(), 2) + Math.pow(point.getY(), 2));
        return c + point.getY() + point.getX();
    }

    @Override
    double getArea() {
        return 0.5 * point.getX() * point.getY();
    }
}
