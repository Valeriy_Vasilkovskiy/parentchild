package Figure;

class Rectangle extends Shape {

    Rectangle(double x, double y) {
        this.point = new Point(x, y);
    }

    private Point point;

    @Override
    double getPerimetr() {
        return point.getX() * 2 + point.getY() * 2;
    }

    @Override
    double getArea() {
        return point.getX() * point.getY();
    }
}
