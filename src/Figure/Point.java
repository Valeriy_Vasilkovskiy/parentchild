package Figure;

class Point {
    private double x;
    private double y;

    Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    Point(double x) {
        this.x = x;
    }

    double getX() {
        return x;
    }

    double getY() {
        return y;
    }
}
