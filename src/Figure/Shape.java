package Figure;

abstract class Shape {

    abstract double getPerimetr();

    abstract double getArea();
}
