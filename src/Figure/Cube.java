package Figure;

class Cube extends Shape {

    Cube(double x) {
        this.point = new Point(x);
    }

    private Point point;

    @Override
    double getPerimetr() {
        return point.getX() * 4;
    }

    @Override
    double getArea() {
        return point.getX() * point.getX();
    }
}
