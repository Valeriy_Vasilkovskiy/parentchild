
package Figure;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        boolean a = false;
        while (!a) {
            System.out.println("Выберете фигуру которую хотите создать: \n 1)Cube \n 2)Circle \n 3)Rectangle \n 4)Rectangular Triangle");
            main.cheekInput(new Scanner(System.in).next());
            main.createFigure();
            System.out.println("Что вы хотите узнать об этой фигуре?\n 1)Периметр \n 2)Площадь");
            main.cheekInput(new Scanner(System.in).next());
            if (main.answer == 1) {
                System.out.println(main.arrayList.get(main.arrayList.size() - 1).getPerimetr());
            } else if (main.answer == 2) {
                System.out.println(main.arrayList.get(main.arrayList.size() - 1).getArea());
            }
            System.out.println("Хотите поставить данную фигуру на доску? \n 1)Да \n 2)Нет");
            main.cheekInput(new Scanner(System.in).next());
            if (main.answer == 1) {
                System.out.println("Укажите цифру от 1 до 4 , на какое место хотите установить");
                main.cheekInput(new Scanner(System.in).next());
                Plank.getInstance().put(main.arrayList.get(main.arrayList.size() - 1), main.answer);
            }
            System.out.println("Хотите убрать какую-нибудь фигуру с доски? \n 1)Да \n 2)Нет");
            main.cheekInput(new Scanner(System.in).next());
            if (main.answer == 1) {
                System.out.println("Укажите цифру от 1 до 4 какое место хотите освободить \n Сейчас заняты места:");
                System.out.println(Plank.getInstance().infoAboutPlace());
                main.cheekInput(new Scanner(System.in).next());
                Plank.getInstance().outPut(main.answer);
            }
            System.out.println("Хотите узнать сумарную площадь фигур на доске? \n 1)Да \n 2)Нет");
            main.cheekInput(new Scanner(System.in).next());
            if (main.answer == 1) {
                System.out.println(Plank.getInstance().infoAboutPlank());
            }
            System.out.println("Хотите закончить работу? \n 1)Да \n 2)Нет");
            main.cheekInput(new Scanner(System.in).next());
            if (main.answer == 1) {
                a = true;
            }
        }
    }

    private void cheekInput(String string_answer) {
        try {
            answer = Integer.parseInt(string_answer);
        } catch (NumberFormatException e) {
            System.out.println("Введите пожалуйста цифру, а не символьные данные :");
            cheekInput(new Scanner(System.in).next());
        }
    }

    private void createFigure() {
        double x;
        double y;
        /////////////////////////////
        switch (answer) {
            case 1:
                System.out.println("Введите длинну стороны Cube:");
                cheekInput(new Scanner(System.in).next());
                arrayList.add(new Cube(answer));
                break;
            case 2:
                System.out.println("Введите длинну радиуса Circle:");
                cheekInput(new Scanner(System.in).next());
                arrayList.add(new Circle(answer));

                break;
            case 3:
                System.out.println("Введите длинну ширины Rectangle:");
                cheekInput(new Scanner(System.in).next());
                x = answer;
                System.out.println("Введите длинну высоты Rectangle:");
                cheekInput(new Scanner(System.in).next());
                y = answer;
                arrayList.add(new Rectangle(x, y));
                break;
            case 4:
                System.out.println("Введите длинну ширины Rectangular Triangle:");
                cheekInput(new Scanner(System.in).next());
                x = answer;
                System.out.println("Введите длинну высоты Rectangular Triangle:");
                cheekInput(new Scanner(System.in).next());
                y = answer;
                arrayList.add(new RectangularTriangle(x, y));
                break;
            default:
                System.out.println("Такого варианта нет");
                break;
        }
    }

    private int answer = 0;
    private ArrayList<Shape> arrayList = new ArrayList<>();

}
